package main

import (
    "fmt"
    "strings"
)

const (
    SHOT_MISSED = 0
    SHOT_KNOCKED = 1
    SHOT_DESTROYED = 2
)

type Ship struct {
    P1 Coord
    P2 Coord
    Knocks []Coord
    Destroyed bool
}


func (sh *Ship) Parse(coordinates string, matrix_range int) (err error) {
    points := strings.Split(coordinates, " ")
    if len(points) != 2 {
        err = fmt.Errorf("wrong count of ship coordinates: %d", len(points))
        return
    }
    coords := []*Coord{&sh.P1, &sh.P2}
    for k, c := range coords {
        if err = c.Parse(points[k]); err != nil {
            return
        }
    }

    if sh.P1.X > sh.P2.X {
        sh.P1.X, sh.P2.X = sh.P2.X, sh.P1.X
    }
    if sh.P1.Y > sh.P2.Y {
        sh.P1.Y, sh.P2.Y = sh.P2.Y, sh.P1.Y
    }

    if sh.P2.X > matrix_range || sh.P2.Y > matrix_range {
        err = fmt.Errorf("coordinates '%s' out of matrix range: %d", coordinates, matrix_range)
        return
    }
    return
}


func (sh *Ship) CoordinatesAsString() (s string) {
    s = sh.P1.AsString() + " " + sh.P2.AsString()
    return
}


func (sh *Ship) IsDestroyed() bool {
    return sh.Destroyed
}


func (sh *Ship) IsKnocked() bool {
    return len(sh.Knocks) > 0 && !sh.IsDestroyed()
}


func (sh *Ship) Shot(p *Coord) (res int, err error) {
    knocked := p.X >= sh.P1.X && p.X <= sh.P2.X && p.Y >= sh.P1.Y && p.Y <= sh.P2.Y

    if knocked {
        for _, v := range sh.Knocks {
            if *p == v {
                err = fmt.Errorf("this coordinates (%s) already knocked", p.AsString())
                return
            }
        }
        sh.Knocks = append(sh.Knocks, *p)

        // check for destroyed
        if len(sh.Knocks) == (sh.P2.X - sh.P1.X + 1) * (sh.P2.Y - sh.P1.Y + 1) {
            sh.Destroyed = true
            res = SHOT_DESTROYED
        } else {
            res = SHOT_KNOCKED
        }
    } else {
        res = SHOT_MISSED
    }
    return
}

