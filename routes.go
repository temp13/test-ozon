package main

import "net/http"

type Route struct {
    Method string
    Pattern string
    HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes {
    Route {"POST", "/create-matrix", HandlerCreateMatrix},
    Route {"POST", "/ship", HandlerShip},
    Route {"POST", "/shot", HandlerShot},
    Route {"POST", "/clear", HandlerClear},
    Route {"GET", "/state", HandlerState},
}


