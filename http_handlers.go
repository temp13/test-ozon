package main

import (
    "net/http"
    "io/ioutil"
    "encoding/json"
)

type (
    CreateMatrixIn struct {
        Range int `json:"range"`
    }

    ShipIn struct {
        Coordinates string `json:"Coordinates"`
    }

    ShotIn struct {
        Coord string `json:"сoord"`
    }
)


func sendError(w http.ResponseWriter, err error) {
    http.Error(w, "error: " + err.Error(), 400)
}


func sendJson(w http.ResponseWriter, data interface{}) {
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(data)
}


func HandlerCreateMatrix(w http.ResponseWriter, r *http.Request) {
    var body []byte
    var err error
    if body, err = ioutil.ReadAll(r.Body); err != nil {
        sendError(w, err)
	return
    }

    var cm CreateMatrixIn
    if err = json.Unmarshal(body, &cm); err != nil {
        sendError(w, err)
	return
    }

    if err = game.CreateMatrix(cm.Range); err != nil {
        sendError(w, err)
	return
    }
}


func HandlerShip(w http.ResponseWriter, r *http.Request) {
    var body []byte
    var err error
    if body, err = ioutil.ReadAll(r.Body); err != nil {
        sendError(w, err)
	return
    }

    var ship ShipIn
    if err = json.Unmarshal(body, &ship); err != nil {
        sendError(w, err)
	return
    }

    if err = game.Ship(ship.Coordinates); err != nil {
        sendError(w, err)
	return
    }
}


func HandlerShot(w http.ResponseWriter, r *http.Request) {
    var body []byte
    var err error
    if body, err = ioutil.ReadAll(r.Body); err != nil {
        sendError(w, err)
	return
    }

    var shot ShotIn
    if err = json.Unmarshal(body, &shot); err != nil {
        sendError(w, err)
	return
    }

    var res ShotResult
    if res, err = game.Shot(shot.Coord); err != nil {
        sendError(w, err)
	return
    }

    sendJson(w, res)
}


func HandlerClear(w http.ResponseWriter, r *http.Request) {
    game.Clear()
}


func HandlerState(w http.ResponseWriter, r *http.Request) {
    sendJson(w, game.State())
}

