package main

import (
    "testing"
)


func TestShips(t *testing.T) {
    type CoordData = struct {
        Range int
        Txt string
        Err string
    }
    var data = []CoordData{
        {5, "1A 2B,3D 3E", ""},
        {5, "1A 1A,2B 3E", ""},
        {5, "1A 1A,1A 3E,1D 1D", "ships are crossed: 1A 1A and 1A 3E"},
        {1, "1A 2B,3D 3E", "coordinates '1A 2B' out of matrix range: 1"},
    }

    for _, d := range data {
        var ss Ships
        ss.Range = d.Range
        err := ss.Parse(d.Txt)
        var e string
        if err != nil {
            e = err.Error()
        }
        cc := ss.CoordinatesAsString()
        if e != d.Err || e == "" && d.Txt != cc {
            t.Errorf("[%s] err: '%s', (%s)", d.Txt, e, cc)
        }
    }
}



