package main

import (
    "flag"
    "log"
    "net/http"
)

var game = NewGame()

func main() {
    var listenAddr = flag.String("listen", ":8080", "`Address` to listen on")
    flag.Parse()

    if err := http.ListenAndServe(*listenAddr, NewRouter()); err != nil {
        log.Fatal(err)
    }
}

