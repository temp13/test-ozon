package main

import (
    "regexp"
    "fmt"
    "strconv"
    "strings"
    "math"
)


type Coord struct {
    X, Y int
}


var rCoord = regexp.MustCompile(`^\s*([0-9]+)([A-Z]+)\s*$`)

func (c *Coord) Parse(s string) (err error) {
    s = strings.ToUpper(s)
    match := rCoord.FindStringSubmatch(s)
    if match == nil {
        err = fmt.Errorf("wrong format of coordinate: '%s'", s)
        return
    }

    if c.Y, err = strconv.Atoi(match[1]); err != nil || c.Y < 1 {
        err = fmt.Errorf("wrong format of number part in coordinate: '%s'", s)
        return
    }

    asciiBytes := []byte(match[2])
    size := len(asciiBytes)
    var n int64
    // dictionary size
    dsize := int64(byte('Z') - byte('A') + 1)

    for k, v := range asciiBytes {
        // value from 0 to 25 (A-Z)
        byte64 := int64(v) - int64(byte('A'))

        // 26^x
        degree := int64(math.Pow(float64(dsize), float64(size - k - 1)))

        n += degree * (byte64 + 1)
    }

    // MaxInt overflow check
    if n > int64(^uint(0) >> 1) {
        err = fmt.Errorf("wrong format of character part in coordinate: '%s'", s)
    }

    c.X = int(n)
    return
}


func (c *Coord) AsString() (s string) {
    // A-Z = 25 + 1
    dsize := int(byte('Z') - byte('A') + 1)
    xx := ""
    n := c.X
    for {
        // mod 26
        b := (n-1) % dsize
        xx += string(int('A') + b)
        n = (n - b) / dsize
        if n == 0 {
            break
        }
    }
    for _,v := range xx {
        s = string(v) + s
    }
    s = strconv.Itoa(c.Y) + s
    return
}
