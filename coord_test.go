package main

import "testing"



func TestCoordParse(t *testing.T) {
    type CoordData = struct {
        Txt string
        X int
        Y int
        Err string
    }
    var data = []CoordData{
        {"1A", 1, 1, ""},
        {"1AA", 27, 1, ""},
        {"1AAB", 704, 1, ""},
        {"1AAAB", 18280, 1, ""},
        {"15F", 6, 15, ""},
        {"x", 0, 0, "wrong format of coordinate: 'X'"},
        {"0A", 0, 0, "wrong format of number part in coordinate: '0A'"},
        {"-1x", 0, 0, "wrong format of coordinate: '-1X'"},
    }

    for _, d := range data {
        var p Coord
        err := p.Parse(d.Txt)
        var e string
        if err != nil {
            e = err.Error()
        }
        if e != d.Err || p.X != d.X || p.Y != d.Y {
            t.Errorf("'%s' => (%d, %d), err: %s. must be: (%d, %d), err: %s", d.Txt, p.X, p.Y, e, d.X, d.Y, d.Err)
        }
    }
}


func TestCoordAsString(t *testing.T) {
    var data = []string{
        "1A",
        "1AA",
        "15F",
        "1AAA",
        "1345ABCDE",
        "1ZZ",
    }

    for _, d := range data {
        var p Coord
        err := p.Parse(d)
        var e string
        if err != nil {
            e = err.Error()
        }
        s := p.AsString()
        if e != "" || s != d {
            t.Errorf("err: %s, src: '%s', dst: '%s'", e, d, s)
        }
    }
}
