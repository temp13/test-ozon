package main

import (
    "fmt"
    "strings"
)


type (
    ShotResult struct {
        Destroy bool `json:"destroy"`
        Knock bool `json:"knock"`
        End bool `json:"end"`
    }

    Ships struct {
        Range int
        Ships []*Ship
    }
)



func (ss *Ships) Parse(coordinates string) (err error) {
    if ss.Range <= 0 {
           err = fmt.Errorf("range not initialized (%d)", ss.Range)
           return
    }
    if len(ss.Ships) > 0 {
           err = fmt.Errorf("ships already initialized")
           return
    }

    ships := strings.Split(coordinates, ",")
    ss.Ships = make([]*Ship, len(ships))
    for i, ship_coordinates := range ships {
        ss.Ships[i] = &Ship{}
        if err = ss.Ships[i].Parse(ship_coordinates, ss.Range); err != nil {
            return
        }
    }
    err = ss.CheckCrosses()
    return
}


func (ss *Ships) CheckCrosses() (err error) {
    for i := 0; i < len(ss.Ships) - 1; i++ {
        ship1 := ss.Ships[i]
        for j := i + 1; j < len(ss.Ships); j++ {
           ship2 := ss.Ships[j]
           if ss.isCrossed(&ship1.P1, &ship1.P2, &ship2.P1, &ship2.P2) {
               err = fmt.Errorf("ships are crossed: %s and %s", ship1.CoordinatesAsString(), ship2.CoordinatesAsString())
               return
           }
        }
    }
    return
}


func (ss *Ships) isCrossed(p11, p12, p21, p22 *Coord) (res bool) {
    left := Max(p11.X, p21.X)
    top := Max(p11.Y, p21.Y)
    right := Min(p12.X, p22.X)
    bottom := Min(p12.Y, p22.Y)

    width := right - left
    height := bottom - top

    return width >= 0 && height >= 0
}


func (ss *Ships) Clear() {
    ss.Ships = []*Ship{}
}


func (ss *Ships) CoordinatesAsString() (s string) {
    for _, v := range ss.Ships {
        if s != "" {
            s += ","
        }
        s += v.CoordinatesAsString()
    }
    return
}


func (ss *Ships) IsEnd() (res bool) {
    if len(ss.Ships) == 0 {
        return
    }
    res = true
    for _, ship := range ss.Ships {
        if !ship.IsDestroyed() {
            res = false
            break
        }
    }
    return
}


func (ss *Ships) IsInitialized() bool {
    return len(ss.Ships) > 0 && ss.Range > 0
}


func (ss *Ships) Shot(coord string) (res ShotResult, err error) {
    if !ss.IsInitialized() {
       err = fmt.Errorf("not initialized")
       return
    }

    var p Coord
    if err = p.Parse(coord); err != nil {
        return
    }

    if p.X > ss.Range || p.Y > ss.Range {
        err = fmt.Errorf("coordinates: [%s] out of range: %d", p.AsString(), ss.Range)
        return
    }

    if ss.IsEnd() {
        err = fmt.Errorf("game over")
        return
    }

    for _, ship := range ss.Ships {
        var shipRes int
        if shipRes, err = ship.Shot(&p); err != nil {
            return
        }
        if shipRes != SHOT_MISSED {
            res.Knock = true
            res.Destroy = shipRes == SHOT_DESTROYED
            break
        }
    }
    res.End = ss.IsEnd()
    return
}

