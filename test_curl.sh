#!/bin/bash

base_url="http://localhost:8080"

function get {
    HTTP_RESPONSE=$(curl --silent --write-out "HTTPSTATUS:%{http_code}" "$base_url$1")
    HTTP_BODY=$(echo $HTTP_RESPONSE | sed -E 's/HTTPSTATUS\:[0-9]{3}$//')
    HTTP_STATUS=$(echo $HTTP_RESPONSE | tr -d '\n' | sed -E 's/.*HTTPSTATUS:([0-9]{3})$/\1/')
    echo "$HTTP_STATUS	$1	out: $HTTP_BODY"
}

function post {
    HTTP_RESPONSE=$(curl --silent --write-out "HTTPSTATUS:%{http_code}" --data "$2" -X POST "$base_url$1")
    HTTP_BODY=$(echo $HTTP_RESPONSE | sed -E 's/HTTPSTATUS\:[0-9]{3}$//')
    HTTP_STATUS=$(echo $HTTP_RESPONSE | tr -d '\n' | sed -E 's/.*HTTPSTATUS:([0-9]{3})$/\1/')
    echo "$HTTP_STATUS	$1	in: $2	out: $HTTP_BODY"
}

post /create-matrix '{"range":6}'
post /ship '{"Coordinates":"1A 2B,3D 3E"}'

post /shot '{"сoord":"1A"}'
post /shot '{"сoord":"1B"}'
post /shot '{"сoord":"2A"}'
post /shot '{"сoord":"2B"}'

get /state
post /shot '{"сoord":"3A"}'
get /state
post /shot '{"сoord":"3D"}'
get /state
post /shot '{"сoord":"q"}'
post /shot '{"сoord":"3E"}'
get /state
post /shot '{"сoord":"3E"}'

post /clear
get /state
post /shot '{"сoord":"1A"}'

post /ship '{"Coordinates":"1A 1A"}'
post /shot '{"сoord":"1A"}'
post /shot '{"сoord":"1A"}'
get /state

