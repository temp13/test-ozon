package main

import "testing"



func TestGameCreateMatrix(t *testing.T) {
    type CoordData = struct {
        Range int
        Err string
    }
    var data = []CoordData{
        {1, ""},
        {0, "wrong value of range: 0"},
        {-1, "wrong value of range: -1"},
    }

    g := NewGame()
    for _, d := range data {
        err := g.CreateMatrix(d.Range)
        var e string
        if err != nil {
            e = err.Error()
        }
        if e != d.Err || (e == "" && g.Ships.Range != d.Range) {
            t.Errorf("range: %d, err: [%s]. must be err: [%s]", d.Range, e, d.Err)
        }

    }
}



func TestGame(t *testing.T) {
    g := NewGame()

    if res, err := g.Shot("1A"); (err == nil || err.Error() != "not initialized") {
        t.Errorf("err: %s. res: %v", err, res)
    }


    if err := g.CreateMatrix(5); err != nil {
        t.Errorf("err: %s", err)
    }

    if err := g.Ship("1A 2B,3D 3D,1D 1E"); err != nil {
        t.Errorf("err: %s", err)
    }

    if res, err := g.Shot("1A"); (err != nil || res != ShotResult{false, true, false}) {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if st := g.State(); (st != State{3, 0, 1, 1}) {
        t.Errorf("st: %#v", st)
    }

    if res, err := g.Shot("1B"); (err != nil || res != ShotResult{false, true, false}) {
        t.Errorf("err: %s. res: %v", err, res)
    }
    if res, err := g.Shot("2A"); (err != nil || res != ShotResult{false, true, false}) {
        t.Errorf("err: %s. res: %v", err, res)
    }
    if res, err := g.Shot("2B"); (err != nil || res != ShotResult{true, true, false}) {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if st := g.State(); (st != State{3, 1, 0, 4}) {
        t.Errorf("st: %#v", st)
    }

    if res, err := g.Shot("3D"); (err != nil || err == nil && res != ShotResult{true, true, false}) {
        t.Errorf("err: %s. res: %v", err, res)
    }
 
    if res, err := g.Shot("3D"); (err == nil || err.Error() != "this coordinates (3D) already knocked") {
        t.Errorf("err: %s. res: %v", err, res)
    }


    if res, err := g.Shot("1F"); (err == nil || err.Error() != "coordinates: [1F] out of range: 5") {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if res, err := g.Shot("1Z"); (err == nil || err.Error() != "coordinates: [1Z] out of range: 5") {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if res, err := g.Shot("1F"); (err == nil || err.Error() != "coordinates: [1F] out of range: 5") {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if res, err := g.Shot("1D"); (err != nil || res != ShotResult{false, true, false}) {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if res, err := g.Shot("5A"); (err != nil || res != ShotResult{false, false, false}) {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if st := g.State(); (st != State{3, 2, 1, 7}) {
        t.Errorf("st: %#v", st)
    }

    if res, err := g.Shot("1E"); (err != nil || res != ShotResult{true, true, true}) {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if st := g.State(); (st != State{3, 3, 0, 8}) {
        t.Errorf("st: %#v", st)
    }

    if res, err := g.Shot("5B"); (err == nil || err.Error() != "game over") {
        t.Errorf("err: %s. res: %v", err, res)
    }

    g.Clear()

    if res, err := g.Shot("1A"); (err == nil || err.Error() != "not initialized") {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if err := g.CreateMatrix(3); err != nil {
        t.Errorf("err: %s", err)
    }

    if res, err := g.Shot("1A"); (err == nil || err.Error() != "not initialized") {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if err := g.Ship("1A 1A,2B 2B"); err != nil {
        t.Errorf("err: %s", err)
    }

    if err := g.Ship("1A 1A,2B 2B"); (err == nil || err.Error() != "ships already initialized") {
        t.Errorf("err: %s", err)
    }

    if res, err := g.Shot("1A"); (err != nil || res != ShotResult{true, true, false}) {
        t.Errorf("err: %s. res: %v", err, res)
    }

    if st := g.State(); (st != State{2, 1, 0, 1}) {
        t.Errorf("st: %#v", st)
    }

    g.Clear()

    if st := g.State(); (st != State{0, 0, 0, 0}) {
        t.Errorf("st: %#v", st)
    }


}


