package main

import (
    "testing"
)


func TestShip(t *testing.T) {
    type CoordData = struct {
        Range int
        Txt string
        P1, P2 Coord
        Err string
    }
    var data = []CoordData{
        {5, "1A 2B", Coord{1,1}, Coord{2,2}, ""},
        {2, "1A 3C", Coord{1,1}, Coord{3,3}, "coordinates '1A 3C' out of matrix range: 2"},
        {1, "1A 2B", Coord{1,1}, Coord{2,2}, "coordinates '1A 2B' out of matrix range: 1"},
        {3, "1a", Coord{0,0}, Coord{0,0}, "wrong count of ship coordinates: 1"},
        {3, "1b z", Coord{2,1}, Coord{0,0}, "wrong format of coordinate: 'Z'"},
    }

    for _, d := range data {
        var sh Ship
        err := sh.Parse(d.Txt, d.Range)
        var e string
        if err != nil {
            e = err.Error()
        }
        cc := sh.CoordinatesAsString()
        if e != d.Err || e == "" && cc != d.Txt {
            t.Errorf("src: '%s', err: '%s', (%s)", d.Txt, e, cc)
        }
    }
}



