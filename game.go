package main

import (
    "fmt"
)


type (
    State struct {
        ShipCount int `json:"ship_count"`
        Destroyed int `json:"destroyed"`
        Knocked int `json:"knocked"`
        ShotCount int `json:"shot_count"`
    }

    Game struct {
        ShotCount int
        Ships Ships
    }
)


func NewGame() (g *Game) {
    g = &Game{}
    return
}


func (g *Game) CreateMatrix(r int) (err error) {
    g.Clear()
    g.Ships.Range = 0
    if r < 1 {
        err = fmt.Errorf("wrong value of range: %d", r)
        return
    }
    g.Ships.Range = r
    return
}


func (g *Game) Clear() {
    g.ShotCount = 0
    g.Ships.Clear()
}


func (g *Game) Ship(coordinates string) (err error) {
    return g.Ships.Parse(coordinates)
}


func (g *Game) Shot(coord string) (res ShotResult, err error) {
    res, err = g.Ships.Shot(coord)
    if err == nil {
        g.ShotCount++
    }
    return
}


func (g *Game) State() (st State){
    st.ShotCount = g.ShotCount
    st.ShipCount = len(g.Ships.Ships)
    for _, ship := range g.Ships.Ships {
        if ship.IsDestroyed() {
            st.Destroyed++
        }
        if ship.IsKnocked() {
            st.Knocked++
        }
    }
    return
}


